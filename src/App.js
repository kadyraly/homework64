import React, { Component, Fragment } from 'react';

import { Switch, Route} from "react-router-dom";
import About from "./component/About/About";
import Home from "./component/Home/Home";
import Contact from "./component/Contact/Contact";
import Header from "./component/Header/Header";
import Add from "./container/Add/Add";
import Post from "./container/Post/Post";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Header />
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/about" component={About} />
                    <Route path="/contact" component={Contact} />
                    <Route path="/add" component={Add} />
                    <Route path="/posts/:id/edit" component={Add} />
                    <Route path="/posts/:id" component={Post} />
                </Switch>
            </Fragment>

        );
    }
}

export default App;

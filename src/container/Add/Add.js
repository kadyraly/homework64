import React, {Component} from 'react';
import './Add.css';
import axios from '../../axios';

class Add extends Component {
    state = {
        title: '',
        body: '',
        loading: false,
        isEdit: false
    };

    componentDidMount() {
        console.log(this.props, '[Add] did mount')
        const isEdit = this.props.match.url.includes("edit");
        console.log(isEdit, 'IS EDIT')
        if (isEdit) {
            axios.get(`posts/${this.props.match.params.id}.json`)
                .then(response => this.setState({title: response.data.title, body: response.data.body}))
        }
        this.setState({isEdit})
    }


    handlePostChange = event => {
        event.persist();
        this.setState({[event.target.name]: event.target.value});
    };

    postSaveHandler = () => {
        this.setState({loading: true});
        const newPost = {
            title: this.state.title,
            body: this.state.body,
            date: Date()
        };
        if (this.state.isEdit) {
            axios.patch(`posts/${this.props.match.params.id}.json`, newPost)
                .then(() => {
                    this.setState({loading: false});
                    this.props.history.replace('/');
                })
        } else {
            axios.post('/posts.json', newPost)
                .then(() => {
                    this.setState({loading: false});
                    this.props.history.replace('/');
                });
        }

    };

    render () {
        return (
            <div className="Edit">
                <input className="PostInput" type="text" name="title" placeholder="new title" value={this.state.title} onChange={this.handlePostChange} />
                <textarea className="PostTextArea"  name="body" placeholder="enter posts..." value={this.state.body} onChange={this.handlePostChange} />
                <button onClick={this.postSaveHandler}>Save</button>
            </div>
        );
    }

}

export default Add;
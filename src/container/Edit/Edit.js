import React, {Component} from 'react';
import './Edit.css';
import axios from '../../axios';

class Edit extends Component {
    state = {
        posts: {
            title: '',
            body: ''
        },
        loading: false
    };




    postSaveHandler = (e) => {
        e.preventDefault();
        this.setState({loading: true});

        axios.post('/posts.json', this.state.posts).then(() => {
            this.setState({loading: false});
            this.props.history.replace('/');

        });
    };
    render () {
        return (
            <form className="add">
                <input type="text" name="title"  value={this.state.posts.title} onChange={this.handlePostChange} />
                <textarea  name="body"  value={this.state.posts.body} onChange={this.handlePostChange} />
                <button onClick={this.postSaveHandler}>Save</button>
            </form>
        );
    }

}

export default Edit;
import React, {Component}from 'react';
import './Post.css';
import axios from '../../axios';
import {NavLink} from "react-router-dom";

class Post extends Component {
    state = {
        post: null
    };

    componentDidMount() {
        this.getPost();
    };

    getPost = () => {
        axios.get(`/posts/${this.props.match.params.id}.json`)
            .then(response => this.setState({post: response.data}))
    };

    postDeleteHandler = () => {
        axios.delete(`/posts/${this.props.match.params.id}.json`)
            .then(() => this.getPost())
    };

    render () {
        return this.state.post ?
            <div>
                <h2>{this.state.post.title}</h2>
                <p>{this.state.post.body}</p>
                <button onClick= {this.postDeleteHandler}>X</button>
                <button><NavLink activeClassName="selected" to={`/posts/${this.props.match.params.id}/edit`}>Edit</NavLink></button>
            </div>
            : null

    };
}

export default Post;
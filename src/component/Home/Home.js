import React, {Component} from 'react';
import './Home.css'
import axios from '../../axios';

import {Link} from "react-router-dom";

class Home extends Component {

    state = {
        posts: [],
        loading: false
    };

    componentDidMount() {
        this.getPosts();
    };

    getPosts = () => {
        this.setState({loading: true});
        axios.get('/posts.json')
            .then(response => {
                response.data ?
                this.setState({posts: response.data}) : null
            })
            .finally(() => this.setState({loading: false}))
    };

    render () {
        return (
            <div className="home">
                {Object.keys(this.state.posts).map(postId => (
                    <div className="Post" key={postId}>
                        <span>Created on: {this.state.posts[postId].date}</span>
                        <h2>{this.state.posts[postId].title}</h2>
                        <Link to={`posts/${postId}`}>Read More</Link>
                    </div>
                ))}
            </div>
        );
    }

}

export default Home;
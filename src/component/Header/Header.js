import React from 'react';
import './Header.css';
import {NavLink} from "react-router-dom";

const Header = props => {
    return (
        <div className="Header">
            <ul>
                <li><NavLink activeClassName="selected" exact to="/">Home</NavLink></li>
                <li><NavLink activeClassName="selected" to="/about">About</NavLink></li>
                <li><NavLink activeClassName="selected" to="/contact">Contact</NavLink></li>
                <li><NavLink activeClassName="selected" to="/add">Add</NavLink></li>
            </ul>
        </div>
    )
};

export  default Header;

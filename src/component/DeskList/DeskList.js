import React from 'react';
import {NavLink} from "react-router-dom";

const DeskList =props => {
    return (
        <div>

            <h2>{props.title}</h2>
            <p>{props.body}</p>

            <button onClick= {props.deleted}>X</button>
            <button><NavLink activeClassName="selected" to={`/posts/${props.id}/edit`}>Edit</NavLink></button>
        </div>
    )

};

export default DeskList;